import { Injectable } from '@angular/core';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import { Musica } from '../models/musica.model';

@Injectable()
export class MusicaService {
  musicas: FirebaseListObservable<any[]>;
  
  constructor(private database: AngularFireDatabase) {
    this.musicas = this.database.list('musicas');
  }

  getAllMusics () {
    return this.musicas;
  }

  getMusics(offset, startKey?) {
    return this.database.list('musicas');
  }

  getMusicById(musicId: string) {
    return this.database.object('musicas/' + musicId);
  }

  add(newEvent: Musica) {
    this.musicas.push(newEvent);
  }

  update(MusicToUpdate) {
    var musicInFirebase = this.getMusicById(MusicToUpdate.$key);
    musicInFirebase.update({
      title: MusicToUpdate.title,
      desc: MusicToUpdate.desc,
      video: MusicToUpdate.video
    });
  }

  delete(musicToDelete){
    var musicInFirebase = this.getMusicById(musicToDelete.$key);
    musicInFirebase.remove();
  }

}
