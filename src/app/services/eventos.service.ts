import { Injectable } from '@angular/core';
import { FirebaseListObservable, AngularFireDatabase } from 'angularfire2/database';
import { Evento } from '../models/evento.model';

@Injectable()
export class EventosService {
  eventos: FirebaseListObservable<any[]>;

  constructor(private database: AngularFireDatabase) {
    this.eventos = database.list('eventos');
  }

  getEvents(offset, startKey?) {
    return this.database.list('eventos', {
      query: {
        orderByKey: true,
        startAt: startKey,
        limitToFirst: offset+1
      }
    })
  }

  getAllEvents () {
    return this.eventos;
  }

  getEventById(eventId: string) {
    return this.database.object('eventos/' + eventId);
  }

  add(newEvent: Evento) {
    this.eventos.push(newEvent);
  }

  update(EventToUpdate) {
    var projectInFirebase = this.getEventById(EventToUpdate.$key);
    projectInFirebase.update({
      title: EventToUpdate.title,
      desc: EventToUpdate.desc,
      pais: EventToUpdate.pais,
      ciudad: EventToUpdate.ciudad,
      direccion: EventToUpdate.direccion,
      fecha: EventToUpdate.fecha,
      hora: EventToUpdate.hora,
      img: EventToUpdate.img
    });
  }

  delete(eventToDelete){
    var eventInFirebase = this.getEventById(eventToDelete.$key);
    eventInFirebase.remove();
  }


}
