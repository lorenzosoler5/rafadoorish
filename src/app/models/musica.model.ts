export class Musica {
  constructor(
    public title: string,
    public desc: string,
    public video: string,
  ) {}
}