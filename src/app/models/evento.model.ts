export class Evento {
  constructor(
    public title: string,
    public desc: string,
    public direccion: string,
    public fecha: string,
    public hora: string,
    public img: string
  ) {}
}