import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { MusicaService } from '../services/musica.service';
import * as _ from "lodash";
@Component({
  selector: 'app-musica',
  templateUrl: './musica.component.html',
  styleUrls: ['./musica.component.css'],
  providers: [MusicaService]
})
export class MusicaComponent implements OnInit {
  musics: any[];
  offset = 6;
  nextKey: any; // for next button
  subscription: any;

  constructor(private musicsService: MusicaService) {
    this.musics = [];
  }

  ngOnInit() {
    this.getMusics()
  }

  nextPage() {
    this.getMusics(this.nextKey)
  }

  private getMusics(key?) {
    this.subscription = this.musicsService.getMusics(this.offset, key).subscribe(musics2 => {
      this.musics = musics2.reverse();;
      // var pagination = _.slice(musics2, 0, this.offset);
      // this.musics = this.musics.concat(...pagination);
      // this.nextKey =_.get(musics2[this.offset], '$key')
      // // this.musics = this.musics.reverse();
    })
  }

}
