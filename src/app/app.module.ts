import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HomeComponent } from './home/home.component';
import { routing } from './app.routing';
import { EventosComponent } from './events/events.component';
import { AdminComponent } from './admin/admin.component';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { LoginComponent } from './login/login.component';
import { BioComponent } from './bio/bio.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ContactoComponent } from './contacto/contacto.component';
import { AutoplayVideoDirective } from './myautoplay.directive';
import { MusicaComponent } from './musica/musica.component';
import { SafePipe } from './safe.pipe';

export const firebaseConfig = {
  apiKey: environment.apiKey,
  authDomain: environment.authDomain,
  databaseURL: environment.databaseURL,
  storageBucket: environment.storageBucket
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    EventosComponent,
    AdminComponent,
    LoginComponent,
    BioComponent,
    ContactoComponent,
    AutoplayVideoDirective,
    MusicaComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    routing,
    NgbModule.forRoot(),
    ScrollToModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    FormsModule,
    AngularFontAwesomeModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
