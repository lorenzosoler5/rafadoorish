import { Component, OnInit } from '@angular/core';
import { EventosService } from '../services/eventos.service';
import { FirebaseListObservable } from 'angularfire2/database';
import { Evento } from '../models/evento.model';
import * as _ from "lodash";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [EventosService]
})
export class EventosComponent implements OnInit {
  events: Evento[] = [];
  offset = 6;
  nextKey;

  constructor(private eventssService: EventosService) { }

  ngOnInit() {
    this.getEvents();
  }

  getEvents (key?) {
    this.eventssService.getEvents(this.offset, key).subscribe((events) => {
      var pagination = _.slice(events, 0, this.offset);
      this.events = this.events.concat(...pagination);
      this.nextKey =_.get(events[this.offset], '$key');
      this.events = this.events.reverse();
    });
  }

  nextPage() {
    this.getEvents(this.nextKey)
  }

  openImage (ev: Evento) {
    window.open(ev.img, 'blank')
  }
}
