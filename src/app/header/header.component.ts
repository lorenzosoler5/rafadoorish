import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  active: boolean = false;
  menuopen: boolean = false;
  constructor (){}

  menuToggle () {
    this.active = !this.active;
    this.menuopen = !this.menuopen;
  }
}
