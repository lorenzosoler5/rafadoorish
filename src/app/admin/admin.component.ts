import { Component, OnInit, EventEmitter } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { LoginService } from '../services/login.service';

import * as firebase from "firebase";
import { EventosService } from '../services/eventos.service';
import { MusicaService } from '../services/musica.service';
import { Evento } from '../models/evento.model';
import { Event } from '@angular/router';
import { Musica } from '../models/musica.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [EventosService, MusicaService, LoginService]
})
export class AdminComponent implements OnInit {
  user;
  events: FirebaseListObservable<any[]>;
  musics: FirebaseListObservable<any[]>;
  // event CMS properties
  showEventCmsControls: boolean = false;
  showAddEventsForm: boolean = false;
  selectedEvent = null;
  // music cms properties
  showMusicCmsControls: boolean = false;
  showAddMusicsForm: boolean = false;
  selectedMusic = null;

  constructor(
    private eventsService: EventosService,
    private musicsService: MusicaService,
    public loginService: LoginService) {}

  ngOnInit() {
    this.events = this.eventsService.getAllEvents();
    this.musics = this.musicsService.getAllMusics();
  }

  hideAllForms() {
    this.showAddEventsForm = false;
    this.showAddMusicsForm = false;
    this.cancelEventEdit();
    this.cancelMusicEdit();
  }

  hideAllCmsControls() {
    this.showEventCmsControls = false;
    this.showMusicCmsControls = false;
  }

  // Event methods
  toggleShowEventCmsControls() {
    this.hideAllForms();
    this.hideAllCmsControls();
    this.showEventCmsControls = !this.showEventCmsControls;
  }

  toggleAddEventsForm(event) {
    if (event) event.preventDefault();
    this.showAddEventsForm = !this.showAddEventsForm;
  }

  submitAddEventForm(
    title: string,
    desc: string,
    direccion: string,
    fecha: string,
    hora: string,
    img: string
  ) {
    const newevent: Evento = new Evento(title, desc, direccion, fecha, hora, img);
    this.eventsService.add(newevent);
    this.hideAllForms();
  }

  editEventClicked(eventToEdit) {
    this.selectedEvent = eventToEdit;
  }

  cancelEventEdit() {
    this.selectedEvent = null;
  }

  updateEventClicked(eventToUpdate) {
    this.eventsService.update(eventToUpdate);
    this.cancelEventEdit();
  }

  deleteEventClicked(eventToDelete) {
    if (confirm("Estas seguro que deseas eliminar este Evento?")) {
      this.eventsService.delete(eventToDelete);
    }
  }
  ///////////////////

  // Music methods
  toggleShowMusicCmsControls() {
    this.hideAllForms();
    this.hideAllCmsControls();
    this.showMusicCmsControls = !this.showMusicCmsControls;
  }

  toggleAddMusicsForm(event) {
    if (event) event.preventDefault();
    this.showAddMusicsForm = !this.showAddMusicsForm;
  }

  submitAddMusicForm(
    title: string,
    desc: string,
    video: string
  ) {
    const newmusic: Musica = new Musica(title, desc, video);
    this.musicsService.add(newmusic);
    this.hideAllForms();
  }

  editMusicClicked(musicToEdit) {
    this.selectedMusic = musicToEdit;
  }

  cancelMusicEdit() {
    this.selectedMusic = null;
  }

  updateMusicClicked(musicToUpdate) {
    this.musicsService.update(musicToUpdate);
    this.cancelMusicEdit();
  }

  deleteMusicClicked(musicToDelete) {
    if (confirm("Estas seguro que deseas eliminar esta Musica?")) {
      this.musicsService.delete(musicToDelete);
    }
  }
  ////////////////////////

  ngDoCheck() {
    this.user = firebase.auth().currentUser;
  }

  runLogout() {
    this.loginService.logout();
  }
}
